class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    puts "Choose a secret word"
    referee.pick_secret_word
    length = referee.pick_secret_word
    guesser.register_secret_length(length)
    @board = "_" * length
  end

  def take_turn
    puts "Please make a guess"
    guess = guesser.guess
    indices = referee.check_guess(guess)
    update_board(guess)
    guesser.handle_response(guess, indices)
  end

  def update_board(guess)
    referee.check_guess(guess).each do |idx|
      board[idx] = guess
    end
  end
end

class HumanPlayer

  attr_accessor :name, :guess
end

class ComputerPlayer

  attr_accessor :dictionary, :secretword, :guess, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secretword = dictionary.sample
    @secretword.length
  end

  def check_guess(el)
    indexes = []
    @secretword.chars.each_with_index do |letter, idx|
      indexes << idx if letter == el
    end
    indexes
  end

  def guess(board)
    counts = Hash.new(0)

    @candidate_words.each do |word|
      word.chars.each {|el| counts[el] += 1 }
    end
    counts.max_by {|k, v| v }[0]
  end

  def handle_response(guess, indicies)
    deletes = []
    if indicies.empty?
      @candidate_words.reject! { |word| word.include?(guess) }
    else
      @candidate_words.each do |word|
        indicies.each do |idx|
          deletes << word if word[idx] != guess
        end
      end
      @candidate_words.reject! { |word| deletes.include?(word) }
    end
    @candidate_words
  end

  def register_secret_length(length)
    @candidate_words = dictionary.select { |word| word.length == length }
  end
end
